public class TestExceptions {
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Lola", "Wolf", "Troy", "Maria","Fingers"};
        try {
            
        for(int i = 0; i< arrayOfAccounts.length; i++){
           arrayOfAccounts[i] = new Account();
           arrayOfAccounts[i].setBalance(amounts[i]);
           arrayOfAccounts[i].setName(names[i]);
           System.out.println('\n' + arrayOfAccounts[i].getName() + 
              " Initial Balance= $" + arrayOfAccounts[i].getBalance());
           arrayOfAccounts[i].addInterest();
           System.out.println("New Balance after added Interest= $" + arrayOfAccounts[i].toString());

           Account.setInterestRate(5.5);
           System.out.println("Static Interest Rate = " + Account.getInterestRate());

           arrayOfAccounts[i].withdraw(2500);
          // System.out.println("Remaining Balance = " + arrayOfAccounts[i].getRemaianingBalance() + '\n');
        }
    } catch (DodgyNameException e) {
        System.out.println("Dodgy Exception " + e.toString());
        return;
    } finally {
        double taxCollectedAmt = 0;
        for (int i = 0; i < arrayOfAccounts.length; i++) {
            if(arrayOfAccounts[i] == null){
                break;
            }
            taxCollectedAmt = taxCollectedAmt + (arrayOfAccounts[i].getBalance() / 100 * 40);
            arrayOfAccounts[i].setBalance(arrayOfAccounts[i].getBalance() / 100 * 60);
        }
        System.out.println("Tax collected = $" + taxCollectedAmt);
    }
    }
}